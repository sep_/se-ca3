import java.time.LocalTime;

public class EnrollCourseEvent extends BaseEvent{
    private BaseEvent baseEvent;
    private Student student;
    private AddedOffering addedOffering;

    public EnrollCourseEvent createEnrollCourseEvent(String request) {
        this.baseEvent = new BaseEvent();
        return name;
    }

    public EnrollCourseEvent removeEnrollCourseEvent(String request) {
        this.baseEvent = new BaseEvent();
        return name;
    }
}


public class ChangeCourseCapacityEvent extend BaseEvent{
    private BaseEvent baseEvent;
    private Admin admin;
    private Offering offering;
    private int oldCapacity;
    private int newCapacity;

    public ChangeCourseCapacityEvent createChangeCourseCapacityEvent(String request) {
        this.baseEvent = new BaseEvent();
        return name;
    }
}

public class ChangeCourseTimeEvent extend BaseEvent{
    private BaseEvent baseEvent;
    private Admin admin;
    private Offering offering;
    private LocalTime oldTime;
    private LocalTime newTime;

    public ChangeCourseTimeEvent createChangeCourseTimeEvent(String request) {
        this.baseEvent = new BaseEvent();
        return name;
    }
}
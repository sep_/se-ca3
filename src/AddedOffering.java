public class AddedOffering {
    Course course;
    Status status = Status.non_finalized;
    boolean wantsToRemove = false;
    boolean isWaiting = false;

    public AddedOffering(Course course, boolean waiting) {
        this.course = course;
        this.isWaiting = waiting;
    }

    public enum Status {
        finalized,
        non_finalized
    }

    public void makeFinalize() {
        if (this.status == Status.non_finalized)
//            this.course.increaseSignedUp();

        this.status = Status.finalized;
    }

    public Status getFinalized() {
        return status;
    }

    public Course getCourse() {
        return this.course;
    }

    public String getStatus() {
        if (isWaiting)
            return "Waiting";
        return "Enrolled";
    }

    public void setToRemove() {
        this.wantsToRemove = true;
    }

    public void cancelRemoving() {
        this.wantsToRemove = false;
    }

    public boolean isWantsToRemove() {
        return wantsToRemove;
    }

    public void changeWaitingToFalse() { this.isWaiting = false; }
}
